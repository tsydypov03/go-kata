package main

import "fmt"

type Project struct {
	Name  string
	Stars int
}

func main() {
	projects := []Project{
		{
			Name:  "chatgpt_academic",
			Stars: 3276,
		},
		{
			Name:  "cursor",
			Stars: 2301,
		},
		{
			Name:  "floatui",
			Stars: 221,
		},
		{
			Name:  "dolly",
			Stars: 740,
		},
		{
			Name:  "browser-agent",
			Stars: 118,
		},
		{
			Name:  "ChatGPT-Next-Web",
			Stars: 1612,
		},
		{
			Name:  "haoel.github.io",
			Stars: 402,
		},
		{
			Name:  "BlenderGPT",
			Stars: 683,
		},
		{
			Name:  "UnityMLStableDiffusion",
			Stars: 57,
		},
		{
			Name:  "BingGPT",
			Stars: 317,
		},
		{
			Name:  "Ventoy",
			Stars: 113,
		},
		{
			Name:  "Text2Video-Zero",
			Stars: 169,
		},
		{
			Name:  "Chinese-alpaca-lora",
			Stars: 226,
		},
	}

	githubProjects := make(map[string]Project)
	for i := range projects {
		githubProjects[projects[i].Name] = projects[i]
	}

	for i := range githubProjects {
		fmt.Println(githubProjects[i])
	}
}
