package main

import (
	"fmt"
	"reflect"
)

const someAddressOffset = 758392

func main() {
	sh := reflect.SliceHeader{
		Data: 0,
		Len:  10,
		Cap:  someAddressOffset,
	}
	fmt.Println(sh)

	doubleSlice()
}

func doubleSlice() {
	slice := make([]int, 10, 15)
	fmt.Printf("len: %d, cap:%d\n", len(slice), cap(slice))
	newSlice := make([]int, len(slice), 2*cap(slice))
	copy(newSlice, slice)
	fmt.Printf("len: %d, cap:%d\n", len(slice), cap(slice))
}
