package main

import "fmt"

type User struct {
	Name string
	Age  int
}

func main() {
	users := []User{
		{
			Name: "Eva",
			Age:  13,
		},
		{
			Name: "Victor",
			Age:  28,
		},
		{
			Name: "Dex",
			Age:  34,
		},
		{
			Name: "Billy",
			Age:  21,
		},
		{
			Name: "Foster",
			Age:  29,
		},
	}

	subUsers := append(users[:0:0], users...)
	subUsers = subUsers[2:len(users)]
	editSecondSlice(subUsers)

	fmt.Println(users)
	fmt.Println(subUsers)
}

func editSecondSlice(subUsers []User) {
	for i := range subUsers {
		subUsers[i].Name = "unknown"
	}
}
