package main

import "fmt"

func main() {
	s := []int{1, 2, 3}
	newSlice := Append(s)
	fmt.Println(newSlice)
}

/*
// Append First way
func Append(s []int) []int {
	s = append(s, 4)
	return s
}
*/

// Append Second way
func Append(s []int) []int {
	s1 := []int{4}
	s = append(s, s1...)
	return s
}
