package main

import "fmt"

type User struct {
	ID   int
	Name string
}

func (u User) GetName(string) string {
	return u.Name
}

type Userer interface {
	GetName(username string) string
}

func main() {
	var i Userer = &User{}
	var name string
	_ = i.GetName(name)
	fmt.Println("Success!")
}
