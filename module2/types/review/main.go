package main

import (
	"fmt"
	"math/rand"
	"sort"
	"time"
)

func init() {
	rand.Seed(time.Now().UnixNano())
}

// User создай структуру User.
type User struct {
	Name   string
	Age    int
	Income int
}

// generateAge сгенерируй возраст от 18 до 70 лет.
func generateAge() int {
	// используй rand.Intn()
	age := rand.Intn(70 - 18 + 1)
	return age + 18
}

// generateIncome сгенерируй доход от 0 до 500000.
func generateIncome() int {
	// используй rand.Intn()
	income := rand.Intn(500000)
	return income
}

// generateFullName сгенерируй полное имя. например "John Doe".
func generateFullName() string {
	// создай слайс с именами и слайс с фамилиями.
	// используй rand.Intn() для выбора случайного имени и фамилии.
	names := []string{"John", "David", "Michael", "James"}
	surnames := []string{"Smith", "Blake", "Stevens", "Lee"}
	name := names[rand.Intn(len(names))]
	surname := surnames[rand.Intn(len(surnames))]
	fullName := name + surname
	return fullName
}

func main() {
	// Сгенерируй 1000 пользователей и заполни ими слайс users.
	users := []User{}
	for i := 0; i <= 1000; i++ {
		user := User{
			Name:   generateFullName(),
			Age:    generateAge(),
			Income: generateIncome(),
		}
		users = append(users, user)
	}
	// Выведи средний возраст пользователей.
	sumAge := 0
	for i := 0; i <= len(users)-1; i++ {
		num := users[i].Age
		sumAge = sumAge + num
	}
	averageAge := sumAge / len(users)
	fmt.Println(averageAge)

	// Выведи средний доход пользователей.
	sumIncome := 0
	for i := 0; i <= len(users)-1; i++ {
		num1 := users[i].Income
		sumIncome = sumIncome + num1
	}
	averageIncome := sumIncome / len(users)
	fmt.Println(averageIncome)

	// Выведи количество пользователей, чей доход превышает средний доход.
	averageIncomeUsers := []User{}
	for i := 0; i <= len(users)-1; i++ {
		if users[i].Income > averageIncome {
			averageIncomeUsers = append(averageIncomeUsers, users[i])
		}
	}

	sort.Slice(averageIncomeUsers, func(i, j int) bool {
		return averageIncomeUsers[i].Age < averageIncomeUsers[j].Age
	})

	fmt.Println(averageIncomeUsers)
}
