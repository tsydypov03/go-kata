package main

import "fmt"

func main() {
	var numberInt int = 3
	var numberFloat float32 = float32(numberInt)
	fmt.Printf("Type: %T, value: %v\n", numberFloat, numberFloat)
}
