package main

import (
	"fmt"
	"unsafe"
)

func main() {

	typeBool()
}

func typeBool() {
	var b bool
	fmt.Println("Размер в байтах:", unsafe.Sizeof(b))
	var u uint8 = 1                  // Берем uint8 - 8 бит, выставляем значение в 1
	fmt.Println(b)                   // bool по умолчанию имеет значение false
	b = *(*bool)(unsafe.Pointer(&u)) // Берем значение из uint8, проставим в тип bool
	fmt.Println(b)
}
