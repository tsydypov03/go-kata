package main

import (
	"fmt"
	"unsafe"
)

func main() {
	n := 112358132134
	fmt.Println("Size is:", unsafe.Sizeof(n), "bytes")
	typeInt()
}

func typeInt() {
	fmt.Println("=== START type int ===")
	var uint8Number uint8 = 1 << 7
	var minInt8 = int8(uint8Number)
	uint8Number--
	var maxInt8 = int8(uint8Number)
	fmt.Println("int8 min value", minInt8, "int8 max value", maxInt8, "size:", unsafe.Sizeof(minInt8), "bytes")

	var uint16Number uint16 = 1 << 15
	var minInt16 = int16(uint16Number)
	uint16Number--
	var maxInt16 = int16(uint16Number)
	fmt.Println("int16 min value", minInt16, "int16 max value", maxInt16, "size:", unsafe.Sizeof(minInt16), "bytes")

	var uint32Number uint32 = 1 << 31
	var minInt32 = int32(uint32Number)
	uint32Number--
	var maxInt32 = int8(uint32Number)
	fmt.Println("int32 min value", minInt32, "int32 max value", maxInt32, "size:", unsafe.Sizeof(minInt32), "bytes")

	var uint64Number uint64 = 1 << 63
	var minInt64 = int64(uint64Number)
	uint64Number--
	var maxInt64 = int64(uint64Number)
	fmt.Println("int64 min value", minInt64, "int64 max value", maxInt64, "size:", unsafe.Sizeof(minInt64), "bytes")
	fmt.Println("=== END type int ===")
}
